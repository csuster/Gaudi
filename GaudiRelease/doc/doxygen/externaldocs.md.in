Related external libraries    {#externaldocs}
==========================

From [LCG version @heptools_version@][lcg-details]:

- LCG Application Area:
  - [CORAL @CORAL_config_version@][coral] (relational access)
  - [COOL @COOL_config_version@][cool] (conditions database)
  - [ROOT @ROOT_config_version@][root] (persistency)
  - [@RELAX_config_version@][relax] (common dictionaries for I/O and interactivity)

- Externals:
  - [Boost @Boost_version@](http://www.boost.org/doc/libs/@Boost_url_version@/)
  - [AIDA @AIDA_config_version@](http://aida.freehep.org/doc/v@AIDA_config_version@/api/) (histogramming)
  - [Xerces-C @XercesC_config_version@](http://xml.apache.org/xerces-c/api-@XercesC_major_version@)
  - [GNU Scientific Library (GSL) @GSL_config_version@](http://www.gnu.org/software/gsl/manual/html_node/index.html)
  - [Python @Python_config_version@](http://www.python.org/doc/@Python_config_version@/) (scripting and interactivity)
  - [HepMC @HepMC_config_version@](http://lcgapp.cern.ch/project/simu/HepMC/)


See also:
  - GNU Standard Template Library
    - [gcc 4.8.1](http://gcc.gnu.org/onlinedocs/gcc-4.8.1/libstdc++/api/)
  - C++ Reference
    - <http://en.cppreference.com/w/cpp>
    - <http://www.cplusplus.com/reference/>


[lcg-details]: http://lcgsoft.web.cern.ch/lcgsoft/release/@heptools_version@/
[coral]: https://twiki.cern.ch/twiki/bin/view/Persistency/Coral
[cool]: https://twiki.cern.ch/twiki/bin/view/Persistency/Cool
[root]: http://root.cern.ch/
[relax]: https://twiki.cern.ch/twiki/bin/view/LCG/RELAX
