// ============================================================================
// Include files
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTupleAlg.h"
// ============================================================================

// ============================================================================
/*  @file GaudiTupleAlg.cpp
 *
 *  Implementation file for class : GaudiTupleAlg
 *
 *  @date 2004-01-23
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 */
// ============================================================================

// ============================================================================
// Force creation of templated class
#include "GaudiAlg/GaudiTuples.icpp"
template class GaudiTuples<GaudiHistoAlg> ;
// ============================================================================

// ============================================================================
// The END
// ============================================================================
