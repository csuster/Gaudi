// ============================================================================
#ifndef GAUDIPYTHON_GAUDIPYTHON_H 
#define GAUDIPYTHON_GAUDIPYTHON_H 1
// ============================================================================
// Include files
// ============================================================================

/** @namespace GaudiPython GaudiPython.h GaudiPython/GaudiPython.h
 *
 *  The general namespace to hide all Gaudi-python related classes  
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@lapp.in2p3.fr 
 *  @date   2005-08-04
 */
namespace GaudiPython 
{
} // end of namespace GaudiPython

#endif // GAUDIPYTHON_GAUDIPYTHON_H
